<nav class="blue-grey lighten-2" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="<?= PUBLIC_PATH; ?>bug" class="brand-logo"> Home </a>
        <ul class="right hide-on-med-and-down">
        
            <?php
            if (isset($_SESSION['user'])) {
                $user = unserialize($_SESSION['user']);
            ?>
                <li>
                    <a href="#" class="user">
                        <img src="<?= PUBLIC_PATH; ?>images/avatar.svg" alt="" class="circle responsive-img">
                        <span class="username"><?= $user->getNom(); ?></span>
                    </a>

                </li>

                <li>
                    <a href="logout" class="user">
                        Logout
                    </a>
                </li>
            <?php
            }
            ?>
        </ul>
        <ul id="nav-mobile" class="sidenav">
            <li><a href="#"></a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>