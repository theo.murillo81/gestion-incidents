<head>
  <?php
  include(__DIR__ . './../header.php');
  include(__DIR__ . './../nav.php');
  ?>
</head>

<div class="section no-pad-bot" id="index-banner">

  <div class="container">
    <br><br>
    <h3 class="light-blue-text flow-text-4">Rapport incident</h3>
  </div>

</div>

<br>
<div class="container">

  <div class="section">

    <form class="col s12" action="#" method="post">
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="" id="title" name="title" type="text" class="validate" required>
          <label for="title">Nom de l'incident</label>
        </div>
        <div class="input-field col s6">
          <input placeholder="" id="date" type="date" name="createdAt" class="validate" required>
          <label for="date">Date</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <textarea placeholder="" id="description" name="description" class="materialize-textarea" type="text" required></textarea>
          <label for="description">Description</label>
        </div>
      </div>
      <button class="waves-effect waves-light btn light-blue right" type="submit" name="submit">Submit <i class="material-icons right">send</i></button>
    </form>
    <br>
  </div>
  <br><br>
</div>

<?php
include(__DIR__ . './../footer.php');
?>

</html>