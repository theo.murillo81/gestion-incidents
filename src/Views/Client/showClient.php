<?php

/** @var $bug \BugApp\Models\Bug */

$bug = $parameters['bug'];

?>

<head>
  <?php
  include(__DIR__ . './../header.php');
  include(__DIR__ . './../nav.php');
  ?>
</head>

<body>
  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br>
      <a href="<?= PUBLIC_PATH; ?>bug" class="waves-effect waves-light btn pulse grey white-text"><i class="material-icons left">arrow_back</i>Retour à la liste</a>
      <br>
      <h3 class="light-blue-text text-4">Fiche description incident</h3>
    </div>

  </div>

  <br>
  <div class="container">

    <div class="section">

      <div class="row">
        <div class="col s6">
          <label for="title">Nom de l'incident</label>
          <p><?= $bug->getTitle(); ?></p>
        </div>
        <div class="col s6">
          <label for="date">Date</label>
          <p><?php echo $bug->getCreatedAt()->format("d/m/Y"); ?></p>
        </div>
      </div>
      <div class="row">
        <div class="col s12">
          <label for="description">Description</label>
          <p style="text-align: justify;"><?= $bug->getDescription(); ?></p>
        </div>
      </div>

    </div>
    <br><br>
  </div>
</body>
<?php
include(__DIR__ . './../footer.php');
?>