<?php

/** @var $bug \BugApp\Models\Bug */
$bugs = $parameters['bugs'];

?>

<?php
include(__DIR__ . './../header.php');
include(__DIR__ . './../nav.php');
?>

<body>
  <div class="section no-pad-bot" id="index-banner">

    <div class="container">
      <br><br>
      <h3 class="blue-grey-text text-darken-4">Liste des incidents</h3>
    </div>

  </div>

  <br>
  <div class="container">

    <div class="flow-text light-blue-text"><a href="<?= PUBLIC_PATH; ?>bug/add" class="btn-floating pulse btn-large waves-effect waves-light blue"><i class="material-icons">add</i></a>  Rapporter un incident</div>

    <div class="row center">
      <table id="table" class="highlight responsive-table">
        <thead>
          <tr>
            <th>id</th>
            <th>Titre</th>
            <th>Date</th>
            <th>Clôture</th>
            <th>Détail</th>
          </tr>
        </thead>

        <tbody>

          <?php foreach ($bugs as $bug) {  ?>

            <tr>
              <td><?= $bug->getId(); ?></td>
              <td><?= $bug->getTitle(); ?></td>
              <td><?php echo $bug->getCreatedAt()->format("d/m/Y"); ?></td>
              <td>
                <?php if ($bug->getClosedAt() != null) {

                  echo $bug->getClosedAt()->format("d/m/Y");
                } else {
                  echo "En Cours";
                }
                ?>
              </td>
              <td><a class="waves-effect waves-light btn blue" href="<?= PUBLIC_PATH; ?>bug/show/<?= $bug->getId(); ?>" /><i class="material-icons left">add</i>Afficher</a></td>
            </tr>

          <?php } ?>
        </tbody>
      </table>

    </div>
    <br><br>
  </div>
</body>
<?php
include(__DIR__ . './../footer.php');
?>