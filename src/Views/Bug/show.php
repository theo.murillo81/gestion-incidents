<?php

/** @var $bug \BugApp\Models\Bug */

$bug = $parameters['bug'];

?>

<!DOCTYPE html>

<html>

<head>
    <?php
    include(__DIR__.'./../header.php');
    include(__DIR__.'./../nav.php');
    ?>
</head>

<body>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <div class="navigation">
        <h4>Fiche descriptive d'incident </h4>
        <h6>Nom de l'incident :<?= $bug->getTitle(); ?></h6>
        <h6 class="left">Description de l'incident : <?= $bug->getDescription(); ?></h6>
        <h5 class="right"> Date d'observation : <?php echo $bug->getCreatedAt()->format("d/m/Y"); ?> </h5><br> 
        <?php if ($bug->getClosedAt() != null) {

            echo $bug->getClosedAt()->format("d/m/Y");
        }
        ?> </h5>
      </div>
    </div>
  </div>
  


</body>
<?php
include(__DIR__.'./../footer.php');
?>

</html>