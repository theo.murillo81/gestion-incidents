<!DOCTYPE html>

<html>

<head>
    <?php
  include(__DIR__.'./../header.php');
  include(__DIR__.'./../nav.php');
    ?>
</head>

<body>
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h4 class="left">Rapport d'incident </h4><br>
            <div class="row">
                <form class="col s12" method="post">
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">bug_report</i>
                            <input name="titre" id="titre" type="text" class="validate" required>
                            <label for="titre">Nom de l'incident </label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">event_note</i>
                            <input name="date" id="date" type="date" class="validate" required>
                            <label for="date">Date d'observation</label>
                        </div>
                        <div class="input-field col s6">
                            <i class="material-icons prefix">edit</i>
                            <textarea name="desc" id="desc" class="materialize-textarea" required></textarea>
                            <label for="desc">Description de l'incident</label>
                        </div>
                    </div>
                    <input name='submit' class="waves-effect waves-light btn" type="submit"></input>
                </form>
            </div>
        </div>
        <br><br>

    </div>
    </div>
</body>
<?php
include(__DIR__.'./../footer.php');
?>

</html>