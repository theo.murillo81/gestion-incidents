<?php

$bugs = $parameters['bugs'];

?>

<!DOCTYPE html>

<html>
<?php
include(__DIR__.'./../header.php');
include(__DIR__.'./../nav.php');
?>

<body>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h1 class="header center blue-grey-text text-darken-4">Liste des Incidents</h1>
      <h4> <a href="http://localhost/exercices/application-de-gestion-d-incidents/public/bug/add" class="btn-floating btn-large waves-effect waves-light blue"><i class="material-icons">add</i></a><a>
          Rapporter un incident </a></h4>
      <div class="row center">
        <h5 class=" highlight responsive-table">
          <table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Sujet</th>
                <th>Date</th>
                <th>Cloture</th>
                <th> </th>
              </tr>
            </thead>
            <?php

            foreach ($bugs as $bug) {
              echo "<tr>";
              echo "<td>" . $bug->getId() . "</td>";
              echo "<td>" . $bug->getTitle() . "</td>";
              echo "<td>" . $bug->getCreatedAt()->format("d/m/Y") . "</td>";
              echo "<td>" . $bug->getClosedAt() . "</td>";
              echo "<td><a class='waves-effect waves-light btn' href='" . PUBLIC_PATH . "bug/show/" . $bug->getId() . "'>Afficher</a></td>";
              echo "</tr>";
            }

            ?>
          </table>
        </h5>
      </div>
      <br><br>

    </div>
  </div>
</body>
<?php
include(__DIR__.'./../footer.php');
?>

</html>