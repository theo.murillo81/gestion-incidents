<?php

/** @var $bug \BugApp\Models\Bug */
$bugs = $parameters['bugs'];

?>

<html>
<?php
include(__DIR__ . './../header.php');
include(__DIR__ . './../nav.php');
?>

<div class="section no-pad-bot" id="index-banner">

  <div class="container">
    <br><br>
    <h3 class="light-blue-text text-4">Liste des incidents</h3>
  </div>

</div>

<br>
<div class="container">

  <div class="section">
    <form>
      <h4 class="light-blue-text text-4"> Filtres</h4>
      <p>
        <label>
          <input type="checkbox" class="filled-in" id="non-cloture" />
          <span>Afficher uniquement les incidents non-cloturés</span>
        </label>
      </p>
      <p>
        <label>
          <input type="checkbox" class="filled-in" id="affiche-assigner" />
          <span>Afficher uniquement les incidents assignés</span>
        </label>
      </p>
    </form>
    <div class="row center">
      <table id="table" class="highlight responsive-table">
        <thead>
          <tr>
            <th>id</th>
            <th>Titre</th>
            <th>Utilisateur</th>
            <th>Date</th>
            <th>Détail</th>
            <th>Ingénieur</th>
            <th>Etat</th>
          </tr>
        </thead>

        <tbody>

          <?php foreach ($bugs as $bug) {  ?>

            <tr>
              <td><?= $bug->getId(); ?></td>
              <td><?= $bug->getTitle(); ?></td>
              <td><?= $bug->getRecorder(); ?></td>
              <td><?php echo $bug->getCreatedAt()->format("d/m/Y"); ?></td>
              <td><a class="waves-effect waves-light btn light-blue" href="<?= PUBLIC_PATH; ?>bug/show/<?= $bug->getId(); ?>" /><i class="material-icons left">add</i>Afficher</a></td>
              <td>
                <?php if ($bug->getEngineer() != null) { ?>
                  <button class="waves-effect waves-light btn light-blue"><i class="material-icons left">person</i><?= $bug->getEngineer(); ?></button>
                <?php } else { ?>
                  <button class= "waves-effect waves-light btn light-blue" value="<?= $bug->getId();?>" id="assigner"><i class="material-icons left">person_add</i>Assigner</button>
                <?php } ?>
              </td>
              <td>
                <?php if ($bug->getClosedAt() != null) { ?>
                  <button class="waves-effect waves-light btn light-blue disabled"><i class="material-icons left">close</i>Cloturer</button>
                <?php } else { ?>
                  <button class="waves-effect waves-light btn light-blue" value="<?= $bug->getId();?>" id="cloturer"><i class="material-icons left">close</i>Cloturer</button>
                <?php } ?>
              </td>
            </tr>

          <?php } ?>
        </tbody>
      </table>

    </div>
    <br><br>
  </div>


  </body>
  <?php
  include(__DIR__ . './../footer.php');
  ?>

</html>