<?php
/** @var $bug \BugApp\Models\Bug */
$bugs = $parameters['bug'];

?>

<!DOCTYPE html>

<html>
<?php
include(__DIR__.'./../header.php');
include(__DIR__.'./../nav.php');
?>

<body>

<div class="section no-pad-bot" id="index-banner">
      <a href="" class="waves-effect waves-light btn-flat blue-grey-text"><i class="material-icons left">chevron_left</i>Retour à la liste</a>
    <div class="container">
      <br>
      <br>
      <h3 class="blue-grey-text text-darken-4">Fiche description incident</h3>
    </div>

    </div>

  <br>
  <div class="container">

    <div class="section">

      <div class="row">
      <div class="col s6">
        <label for="titre">Nom de l'incident</label>
        <p><?=$bug->getTitle();?></p>
      </div>
      <div class="col s6">
        <label for="date">Date</label>
          <p><?php echo $bug->getCreatedAt()->format("d/m/Y");?></p>
      </div>
    </div>
    <div class="row">
      <div class="col s12">
        <label for="desc">Description</label>
        <p style="text-align: justify;"><?=$bug->getDescription();?></p>
      </div>
    </div>

      <form method="post">
          <p>
            <label>
              <input type="checkbox" class="filled-in" checked="checked" name="cloture" />
              <span>Clôture de l'incident</span>
            </label>
          </p>
          <input style="float:right;" class="waves-effect waves-light btn blue-grey" type="submit" value="Enregistrer" name="submit">
      </form>

      <br><br>
    </div>
    <br><br>
  </div>

<?php
include(__DIR__.'./../footer.php');
?>

</html>