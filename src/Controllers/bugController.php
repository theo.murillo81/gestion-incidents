<?php

namespace BugApp\Controllers;

use BugApp\Models\BugManager;
use BugApp\Models\Bug;
use BugApp\Controllers\abstractController;

class bugController extends abstractController
{

    public function show($id)
    {

        // Données issues du Modèle

        $manager = new BugManager();

        $bug = $manager->find($id);
        $type = $_SESSION['type'];

        if ($type == "recorder") {
            $content = $this->render('src/Views/Client/showClient', ['bug' => $bug]);
        }

        if ($type == "engineer") {
            $content = $this->render('src/Views/Engineer/showEngineer', ['bug' => $bug]);
        }

        return $this->sendHttpResponse($content, 200);
    }

    public function index()
    {
        $manager = new BugManager();
        $bugs = $manager->findAll();
        $type = $_SESSION['type'];

        if ($type == "recorder") {
            $content = $this->render('src/Views/Client/listClient', ['bugs' => $bugs]);
        }
        if ($type == "engineer") {
            $content = $this->render('src/Views/Engineer/listEngineer', ['bugs' => $bugs]);
        }

        return $this->sendHttpResponse($content, 200);
    }

    public function add()
    {

        // Ajout d'un incident
        if (isset($_POST['submit'])) {
            // Appel manager + insertion bdd
            $bug = new Bug();
            $bug->setTitle($_POST['title']);
            $bug->setDescription($_POST['description']);
            $bug->setCreatedAt($_POST['date']);

            $manager = new BugManager();
            $manager->add($bug);

            header('Location:' . PUBLIC_PATH . 'bug');
        } else {
            $content = $this->render('src/Views/Client/addClient', []);
            return $this->sendHttpResponse($content, 200);
        }
    }
    public function update($id)
    {

        // Update d'un incident
        $manager = new BugManager();
        $bug = $manager->find($id);

        if (isset($_POST['submit'])) {

            if (isset($_POST['cloture'])) {

                $manager->update($bug);
                header('Location:' . PUBLIC_PATH . 'bug/show/' . $id);
            } else {
                header('Location:' . PUBLIC_PATH . 'bug/update/' . $id);
            }
        } else {
            $content = $this->render('src/Views/Engineer/updateEngineer', ['bug' => $bug]);
            return $this->sendHttpResponse($content, 200);
        }
    }
    public function assign($id)
    {
        $manager = new BugManager();
        $bug = $manager->find($id);
        $username = unserialize($_SESSION['user']);

        $manager->assignation($bug, $username);

        $engineer = [

            "engineer" => $username->getNom()

        ];

        echo json_encode($engineer);
    }

    public function closing($id)
    {
        // UPDATE DATE INCIDENT
        $manager = new BugManager();
        $bug = $manager->find($id);
        $manager->update($bug);

        $closed = [

            "closed" => 'Cloturer'

        ];

        echo json_encode($closed);
    }
}
