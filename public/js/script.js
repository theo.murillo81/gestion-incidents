// RECUPERATION Bouton ASSIGNER
let assigner = document.querySelectorAll("#assigner")

// RECUPERATION DE L'Id DE L'INCIDENT
// POUR CHAQUE BOUTON
assigner.forEach((button) => {
  button.addEventListener('click', (event) => {
    event.preventDefault()
        var id = button.value
        assignation(event, id)
        console.log(id)
  })
})

// FUNCTION AJAX POUR ASSIGNER
function assignation(event, id) {

    // ENVOI L'Id DANS index.php
    httpRequest = new XMLHttpRequest()
    httpRequest.open('GET', 'bug/assign/'+id)
    httpRequest.send()

    // MAJ BOUTON ASSIGNER
    // AJOUTE LE NOM DE L'INGENIEUR
    httpRequest.onreadystatechange = () => {
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
          let object = JSON.parse(httpRequest.responseText)
          console.log(object.engineer)
          event.target.parentElement.innerHTML =
          '<button class="waves-effect waves-light btn light-blue"><i class="material-icons left">person</i>'
          + object.engineer + '</button>'
        }
      }
    }
}

// RECUPERATION Bouton CLOTURER
let cloturer = document.querySelectorAll("#cloturer")

// RECUPERATION DE L'Id DE L'INCIDENT
// POUR CHAQUE BOUTON CLOTURER
cloturer.forEach((button) => {
  button.addEventListener('click', (event) => {
    event.preventDefault()
        var id = button.value
        cloture(event, id)
        console.log(id)
  })
})

// FUNCTION AJAX POUR CLOTURER
function cloture(event, id) {

    // ENVOI L'Id DANS index.php
    httpRequest = new XMLHttpRequest()
    httpRequest.open('GET', 'bug/closing/'+id)
    httpRequest.send()

    // MAJ BOUTON CLOTURER
    httpRequest.onreadystatechange = () => {
      if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
          let object = JSON.parse(httpRequest.responseText)
          console.log(object.closed)
          event.target.parentElement.innerHTML =
          '<button class="waves-effect waves-light btn light-blue disabled"><i class="material-icons left">close</i>'
          + object.closed + '</button>'
        }
      }
    }
}
